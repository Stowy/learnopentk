﻿using OpenToolkit.Windowing.Desktop;

namespace OpenTKBasics
{
    class Program
    {
        static void Main(string[] args)
        {
            GameWindowSettings gameWindowSettings = new GameWindowSettings();

            NativeWindowSettings nativeWindowSettings = new NativeWindowSettings();
            nativeWindowSettings.Size = new OpenToolkit.Mathematics.Vector2i(800, 600);
            nativeWindowSettings.Title = "Learn OpenTK";

            using (Game game = new Game(gameWindowSettings, nativeWindowSettings))
            {
                game.Run();
            }
        }
    }
}
