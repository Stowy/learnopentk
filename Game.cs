using OpenToolkit.Windowing.Desktop;
using OpenToolkit.Windowing.Common;
using OpenToolkit.Windowing.Common.Input;
using OpenToolkit.Graphics.ES30;
using System;

namespace OpenTKBasics
{
    public class Game : GameWindow
    {
        const int POSITION_DATA_SIZE = 3;
        const int TEXTURE_COORD_DATA_SIZE = 2;
        const int VERTEX_LOCATION = 0;

        private readonly float[] vertices =
        {
            //Position          Texture coordinates
            0.5f,  0.5f, 0.0f, 1.0f, 1.0f, // top right
            0.5f, -0.5f, 0.0f, 1.0f, 0.0f, // bottom right
            -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, // bottom left
            -0.5f,  0.5f, 0.0f, 0.0f, 1.0f  // top left
        };

        private readonly uint[] indices = {  // note that we start from 0!
            0, 1, 3,   // first triangle
            1, 2, 3    // second triangle
        };

        private int vertexBufferObject;
        private int vertexArrayObject;
        private int elementBufferObject;

        private Shader shader;
        private Texture texture;

        public Game(GameWindowSettings gameWindowSettings, NativeWindowSettings nativeWindowSettings) : base(gameWindowSettings, nativeWindowSettings)
        {
            // Do nothing
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            KeyboardState input = KeyboardState;

            if (input.IsKeyDown(Key.Escape))
            {
                Close();
            }

            base.OnUpdateFrame(e);
        }

        protected override void OnLoad()
        {
            // Compile the shaders
            shader = new Shader("shaders/shader.vert", "shaders/shader.frag");

            // Load the texture
            texture = new Texture("img/container.png");

            // Set the background color of the window
            GL.ClearColor(0.2f, 0.3f, 0.3f, 1.0f);

            // Set the border color
            float[] borderColor = { 1.0f, 1.0f, 0.0f, 1.0f };
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureBorderColor, borderColor);

            // Create the VAO
            vertexArrayObject = GL.GenVertexArray();
            GL.BindVertexArray(vertexArrayObject);

            // Create the VBO
            vertexBufferObject = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferObject);
            GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StaticDraw);

            // Create the EBO
            elementBufferObject = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, elementBufferObject);
            GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(uint), indices, BufferUsageHint.StaticDraw);

            GL.VertexAttribPointer(VERTEX_LOCATION, POSITION_DATA_SIZE, VertexAttribPointerType.Float, false,
                (POSITION_DATA_SIZE + TEXTURE_COORD_DATA_SIZE) * sizeof(float), shader.GetAttribLocation("aPosition"));
            int texCoordLocation = shader.GetAttribLocation("aTexCoord");
            GL.EnableVertexAttribArray(texCoordLocation);
            GL.VertexAttribPointer(texCoordLocation, TEXTURE_COORD_DATA_SIZE, VertexAttribPointerType.Float, false,
                (POSITION_DATA_SIZE + TEXTURE_COORD_DATA_SIZE) * sizeof(float), POSITION_DATA_SIZE * sizeof(float));

            base.OnLoad();
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            // Clear the background of the window
            GL.Clear(ClearBufferMask.ColorBufferBit);

            // Set to use the shader
            shader.Use();

            texture.Use();

            // Set the VAO to draw
            GL.BindVertexArray(vertexArrayObject);

            // Draw our Square
            GL.DrawElements(PrimitiveType.Triangles, indices.Length, DrawElementsType.UnsignedInt, (IntPtr)0);

            SwapBuffers();
            base.OnRenderFrame(e);
        }

        protected override void OnResize(ResizeEventArgs e)
        {
            GL.Viewport(0, 0, e.Width, e.Height);
            base.OnResize(e);
        }

        protected override void OnUnload()
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.DeleteBuffer(vertexBufferObject);

            shader.Dispose();

            base.OnUnload();
        }
    }
}